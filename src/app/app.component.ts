import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-root',
  template: `
  <nav class="navbar navbar-light bg-faded">
  <a class="navbar-brand" href="#">Admin panel</a>
  <ul class="nav navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="http://example.com" id="supportedContentDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
      <div class="dropdown-menu" aria-labelledby="supportedContentDropdown">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </li>
  </ul>
  <form class="form-inline float-xs-right">
    <input class="form-control" type="text" placeholder="Search">
    <button class="btn btn-outline-success" type="submit">Search</button>
  </form>
</nav>
<br>
  <div *ngIf='tester == false' class='container'>
    <input type="text" class="form-control" #email placeholder="Введите emeil" />
    <br>
    <input type="text" class="form-control" #password placeholder="Введите пароль " />
    <br>
    <button class='btn btn-outline-secondary btn-lg btn-block' (click)="register(email.value,password.value)">Зарегестрироваться</button>
    <hr>
    <input type="text" class="form-control" #emais placeholder="Введите emeil" />
    <br>
    <input type="text" class="form-control" #passwords placeholder="Введите пароль " />
    <br>
    <button class='btn btn-outline-secondary btn-lg btn-block' (click)="login(emais.value,passwords.value)">Войти</button>
    <hr>
    <button class='btn btn-outline-danger' (click)='logOut()'>Покеда</button>
  </div>
  <div class='container' *ngIf='tester == true'>
    <h1>EMAIL:<div class="alert alert-success" role="alert">
  <strong>{{ (user | async)?.email }}</strong>
</div></h1>
<h1>STATUS:<div class="alert alert-success" role="alert">
<strong>{{ (user | async)?.status }}</strong>
</div></h1>
     {{ (user | async)?.email }}
    {{ (user | async)?.status }}
    <hr>
    <button class='btn btn-outline-danger' (click)='logOut()'>Покеда</button>
    <a href='https://www.google.ru/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=%D0%9A%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B1%D1%83%D0%B1%D0%B5%D0%BD%20%D0%B4%D0%BB%D1%8F%20%D0%B8%D0%B7%D0%B3%D0%BD%D0%B0%D0%BD%D0%B8%D1%8F%20%D1%81%D0%BE%D1%82%D0%B0%D0%BD%D1%8B%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D1%82%D1%8E%D0%BC%D0%B5%D0%BD%D1%8C'>На всякий случай</a>
  </div>
  `,
})
export class AppComponent {
  items: FirebaseListObservable<any>;
  emailSubject: Subject<any>;
  user: FirebaseObjectObservable<any>;
  constructor(public af: AngularFire) {
    this.af.auth.subscribe(auth => {
      if(auth){
        console.log('auth success');
        this.user = this.af.database.object('users/' + auth.uid);
        console.log(this.user);
        this.tester = true;
      }
    })
  }
  logOut() {
    this.af.auth.logout();
    this.tester = false;
  }
  overrideLogin() {
    this.af.auth.login({
      provider: AuthProviders.Anonymous,
      method: AuthMethods.Anonymous,
    });
  }
  login(email: string, password: string){
    this.af.auth.login({ email: email, password: password })
    
  }
  register(email: string, password: string){
    this.af.auth.createUser({
      email: email,
      password: password
    }).then(data=>{
      console.log('data',data);
      if(data.auth.email === email ){
        this.af.database.object('users/'+data.auth.uid).set({email:data.auth.email, status: 'user'})
        console.log('В базу отправлено: ' + email);
        console.log('В базу отправлено:' + data.auth.uid);
        console.log('Аккаунт успешно зарегистрирован!');
      }
    });
  }
  tester = false;
}
